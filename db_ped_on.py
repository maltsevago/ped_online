import sqlite3

# Класс создан для того, что бы для работы с базой данных
# В дальнейшем использовать меньше кода
# Конструктор класса требует только путь к файлу+название файла = db_name
class UseDatabase:

    def __init__(self, db_name):
        self.db_name = db_name

    def __enter__(self):
        self.conn = sqlite3.connect(self.db_name)
        self.cur = self.conn.cursor()
        return self.cur

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.conn.commit()
        self.cur.close()
        self.conn.close()

# Название базы данных и путь
# Отсутсвие пути означает, что база будет в корневой папке проекта
DB_NAME = "ped_on.db"

# Таблица Пользователи (Включает Клиентов, Врачей и Администраторов)
create_users = """
CREATE TABLE users 
(id INTEGER PRIMARY KEY,
email VARCHAR(40),
password VARCHAR(15),
status VARCHAR(10),
type VARCHAR(10),
fname VARCHAR(30),
lname VARCHAR(30),
tname VARCHAR(30),
date VARCHAR(15),
city VARCHAR(30),
region VARCHAR(15),
postcode VARCHAR(10),
street VARCHAR(30),
house VARCHAR(5),
flat VARCHAR(5),
phone VARCHAR(5))
"""

# Таблица Дети, содержит id пользователй (родителей)
create_children = """
CREATE TABLE children
(id INTEGER PRIMARY KEY,
u_id INTEGER,
fname VARCHAR(30),
lname VARCHAR(30),
tname VARCHAR(30),
birthday VARCHAR(15),
sex VARCHAR(5),
allergy VARCHAR(150),
extra VARCHAR(150),
height VARCHAR(20),
weight VARCHAR(20))
"""

# Таблица вопросов, содержит id пациента (пользовтаеля, задавшего вопрос)
# Так же содержит id врача (пользователя, ответившего на вопрос)
# Так же имеется id ребенка (Вопрос относится к ребенку)
create_question = """
CREATE TABLE questions
(id INTEGER PRIMARY KEY,
u_id INTEGER,
child_id INTEGER,
doc_id INTEGER,
doc_full_name VARCHAR(50),
text VARCHAR(150),
answer VARCHAR(150),
q_date VARCHAR(15),
a_date VARCHAR(15),
user_full_name VARCHAR(50))
"""