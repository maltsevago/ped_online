from flask import Flask, render_template, request, session
from db_ped_on import UseDatabase
from ped_settings import check_logged

app = Flask(__name__)
app.secret_key = 'Sf7DH&3gRH#7jy_lfo_cjd36f_DF_kdh_hDf_fe'
DB_NAME = "ped_on.db"


@app.route('/')
def main_page():
    return render_template('ped_main.html',
                           the_title='Главная страница',
                           logged=check_logged(session))


@app.route('/registration', methods=['GET', 'POST'])
def registration():
    if request.method == 'POST':
        with UseDatabase(DB_NAME) as cursor:
            _SQL = """
                        INSERT INTO users
                        VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
                        """
            cursor.execute(_SQL, (request.form['email'],
                                  request.form['password'],
                                  'NEW',
                                  'user',
                                  request.form['fname'],
                                  request.form['lname'],
                                  request.form['tname'],
                                  request.form['date'],
                                  request.form['city'],
                                  request.form['region'],
                                  request.form['postcode'],
                                  request.form['street'],
                                  request.form['house'],
                                  request.form['flat'],
                                  request.form['phone'],
                                  ))
        return render_template('ped_login.html',
                               the_title='Войти в кабинет',
                               logged=check_logged(session))
    return render_template('ped_registration.html',
                           the_title='Регистрация',
                           logged=check_logged(session))


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        with UseDatabase(DB_NAME) as cursor:
            _SQL = """
                SELECT email FROM users
                """
            cursor.execute(_SQL)
            data = cursor.fetchall()
            new_list = []
            for d in data:
                new_list.append(d[0])
        email = request.form['email'].strip()
        if email not in new_list:
            return render_template('ped_login.html',
                                   error_text='Неверный email',
                                   the_title='Войти в кабинет',
                                   logged=check_logged(session))
        else:
            with UseDatabase(DB_NAME) as cursor:
                _SQL = """
                SELECT * FROM users
                WHERE email = ?
                """
                cursor.execute(_SQL, (email,))
                data = cursor.fetchall()[0]
                if data[2] != request.form['password']:
                    return render_template('ped_login.html',
                                           error_text='Неверный пароль',
                                           the_title='Войти в кабинет',
                                           logged=check_logged(session))
                else:
                    if data[3] == 'NEW':
                        session['logged_in'] = True
                        session['name'] = data[5]
                        full_name = data[5] + ' ' + data[6] + ' ' + data[7]
                        session['full_name'] = full_name.title()
                        session['u_type'] = data[4]
                        session['status'] = data[3]
                        session['u_id'] = data[0]
                        if data[4] == 'admin':
                            with UseDatabase(DB_NAME) as cursor:
                                _SQL = """
                                SELECT * FROM users
                                """
                                cursor.execute(_SQL)
                                data = cursor.fetchall()
                            return render_template('ped_a_home.html',
                                                   user_name=session['name'],
                                                   user_type=session['u_type'],
                                                   the_title='Личный кабинет',
                                                   users=data,
                                                   logged=check_logged(session))
                        elif data[4] == 'doc':
                            with UseDatabase(DB_NAME) as cursor:
                                _SQL = """
                                SELECT * FROM questions
                                WHERE doc_id != 0
                                """
                                cursor.execute(_SQL)
                                q_p_data = cursor.fetchall()
                            with UseDatabase(DB_NAME) as cursor:
                                _SQL = """
                                SELECT * FROM questions
                                WHERE doc_id = 0
                                """
                                cursor.execute(_SQL)
                                q_data = cursor.fetchall()
                            return render_template('ped_d_home.html',
                                                   user_name=session['name'],
                                                   user_type=session['u_type'],
                                                   the_title='Личный кабинет',
                                                   question_list=q_data,
                                                   question_pass_list=q_p_data,
                                                   logged=check_logged(session))
                        else:
                            with UseDatabase(DB_NAME) as cursor:
                                _SQL = """SELECT * FROM children
                                WHERE u_id = ?
                                """
                                cursor.execute(_SQL, (session['u_id'],))
                                data = cursor.fetchall()
                            with UseDatabase(DB_NAME) as cursor:
                                _SQL = """SELECT * FROM questions
                                WHERE u_id = ?
                                """
                                cursor.execute(_SQL, (session['u_id'],))
                                q_data = cursor.fetchall()
                            return render_template('ped_home.html',
                                                   user_name=session['name'],
                                                   user_type=session['u_type'],
                                                   the_title='Личный кабинет',
                                                   child_list=data,
                                                   question_list=q_data,
                                                   logged=check_logged(session))
                    else:
                        return render_template('ped_login.html',
                                               error_text='Неверный статус',
                                               the_title='Личный кабинет',
                                               logged=check_logged(session))
    return render_template('ped_login.html',
                           the_title='Войти в кабинет',
                           logged=check_logged(session))


@app.route('/logout')
def logout():
    session.pop('logged_in')
    session.pop('name')
    session.pop('full_name')
    session.pop('u_type')
    session.pop('status')
    session.pop('u_id')
    return render_template('ped_login.html',
                           the_title='Войти в кабинет',
                           logged=check_logged(session))


@app.route('/home', methods=['GET', 'POST'])
def home_page():
    if 'logged_in' in session:
        if session['u_type'] == 'admin':
            with UseDatabase(DB_NAME) as cursor:
                _SQL = """
                SELECT * FROM users
                """
                cursor.execute(_SQL)
                data = cursor.fetchall()
            return render_template('ped_a_home.html',
                                   user_name=session['name'],
                                   user_type=session['u_type'],
                                   the_title='Личный кабинет',
                                   users=data,
                                   logged=check_logged(session))
        elif session['u_type'] == 'doc':
            if request.method == 'POST':
                import datetime
                date = datetime.date.today()
                with UseDatabase(DB_NAME) as cursor:
                    _SQL = """
                    UPDATE questions
                    SET
                    doc_id = ?,
                    doc_full_name = ?,
                    answer = ?,
                    a_date = ?
                    WHERE id = ?
                    """
                    cursor.execute(_SQL, (session['u_id'],
                                          session['full_name'],
                                          request.form['answer'],
                                          str(date),
                                          request.form['q_id']
                                          ))
            with UseDatabase(DB_NAME) as cursor:
                _SQL = """
                SELECT * FROM questions
                WHERE doc_id != 0
                """
                cursor.execute(_SQL)
                q_p_data = cursor.fetchall()
            with UseDatabase(DB_NAME) as cursor:
                _SQL = """
                SELECT * FROM questions
                WHERE doc_id = 0
                """
                cursor.execute(_SQL)
                q_data = cursor.fetchall()
            return render_template('ped_d_home.html',
                                   user_name=session['name'],
                                   user_type=session['u_type'],
                                   the_title='Личный кабинет',
                                   question_list=q_data,
                                   question_pass_list=q_p_data,
                                   logged=check_logged(session))
        else:
            if request.method == 'POST':
                if request.form['add'] == 'C':
                    with UseDatabase(DB_NAME) as cursor:
                        _SQL = """
                                    INSERT INTO children
                                    VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
                                    """
                        cursor.execute(_SQL, (session['u_id'],
                                              request.form['f_name'],
                                              request.form['s_name'],
                                              request.form['t_name'],
                                              request.form['birthday'],
                                              request.form['sex'],
                                              request.form['allergy'],
                                              request.form['extra'],
                                              request.form['height'],
                                              request.form['weight']
                                              ))
                elif request.form['add'] == 'Q':
                    import datetime
                    date = datetime.date.today()
                    with UseDatabase(DB_NAME) as cursor:
                        _SQL = """
                                    INSERT INTO questions
                                    VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?)
                                    """
                        cursor.execute(_SQL, (session['u_id'],
                                              request.form['child_id'],
                                              0,
                                              'Врач не ответил',
                                              request.form['question'],
                                              'Ответ отсутствует',
                                              str(date),
                                              'None',
                                              session['full_name'],
                                              ))
            with UseDatabase(DB_NAME) as cursor:
                _SQL = """SELECT * FROM children
                WHERE u_id = ?
                """
                cursor.execute(_SQL, (session['u_id'],))
                data = cursor.fetchall()
            with UseDatabase(DB_NAME) as cursor:
                _SQL = """SELECT * FROM questions
                WHERE u_id = ?
                """
                cursor.execute(_SQL, (session['u_id'],))
                q_data = cursor.fetchall()
            return render_template('ped_home.html',
                                   user_name=session['name'],
                                   user_type=session['u_type'],
                                   the_title='Личный кабинет',
                                   child_list=data,
                                   question_list=q_data,
                                   logged=check_logged(session))
    return render_template('ped_login.html',
                           the_title='Войти в кабинет',
                           logged=check_logged(session))


@app.route('/a_home', methods=['GET', 'POST'])
def admin_home_page():
    if 'logged_in' in session:
        if request.method == 'POST':
            if request.form['edit'] == 'role':
                with UseDatabase(DB_NAME) as cursor:
                    _SQL = """
                    UPDATE users
                    SET type = ?
                    WHERE id = ?
                    """
                    cursor.execute(_SQL, (request.form['result'], request.form['id_user'],))
        with UseDatabase(DB_NAME) as cursor:
            _SQL = """
            SELECT * FROM users
            """
            cursor.execute(_SQL)
            data = cursor.fetchall()
        return render_template('ped_a_home.html',
                               user_name=session['name'],
                               user_type=session['u_type'],
                               the_title='Личный кабинет',
                               users=data,
                               logged=check_logged(session))
    return render_template('ped_login.html',
                           the_title='Войти в кабинет',
                           logged=check_logged(session))


@app.route('/map', methods=['GET', 'POST'])
def map_page():
    from temp_data import TempData
    if 'logged_in' in session:
        if session['u_type'] in ['admin', 'doc']:
            if request.method == 'POST':
                new_house_list = TempData.maps
                new_house_list.append([0,
                                       request.form['inputName'],
                                       request.form['inputCity'],
                                       request.form['inputAddress'],
                                       request.form['inputLink']])
                return render_template('ped_doc_map.html',
                                       the_title='Карта',
                                       logged=check_logged(session),
                                       med_houses=new_house_list
                                       )
            return render_template('ped_doc_map.html',
                                   the_title='Карта',
                                   logged=check_logged(session),
                                   med_houses=TempData.maps
                                   )
    return render_template('ped_map.html',
                           the_title='Карта',
                           logged=check_logged(session),
                           med_houses=TempData.maps
                           )


@app.route('/advice', methods=['GET', 'POST'])
def advice_page():
    from temp_data import TempData
    if 'logged_in' in session:
        if session['u_type'] in ['admin', 'doc']:
            pass
    return render_template('ped_advice.html',
                           the_title='Советы',
                           logged=check_logged(session),
                           advice_list=TempData.advice_list
                           )


@app.route('/about')
def about_page():
    return render_template('ped_about.html',
                           the_title='О нас',
                           logged=check_logged(session)
                           )


app.run(port='9811', debug=True)

